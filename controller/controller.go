package controller

import . "gin_test/domain"
import "gin_test/application"
import "github.com/gin-gonic/gin"
import "net/http"

import "fmt"

// take gin context and set every controller here.
func SetController(engine *gin.Engine) {
	service := application.NewBookService()
	// service := application.BookServiceMock{}

	v1 := engine.Group("/v1")
	{
		v1.GET("/isbn/:isbn", func(c *gin.Context) {
			isbnRaw := c.Param("isbn")
			isbn := ISBN(isbnRaw)
			book, err := service.FindBook(isbn)
			if err != nil {
				c.String(http.StatusInternalServerError, "Server Error")
				return
			}
			c.JSON(http.StatusOK, book)
		})
		v1.GET("/list", func(c *gin.Context) {
			ls, err := service.FindAllBookList()
			if err != nil {
				c.String(http.StatusInternalServerError, "Server Error")
				return
			}
			c.JSON(http.StatusOK, ls)
		})
		v1.POST("/add", func(c *gin.Context) {
			var book Book

			if err := c.BindJSON(&book); err != nil {
				fmt.Println(err)
				c.String(http.StatusInternalServerError, "Server Error")
				return
			}

			if err := service.AddBook(book); err != nil {
				c.String(http.StatusInternalServerError, "Server Error")
				return
			}
			c.JSON(http.StatusCreated, gin.H{})
		})
		v1.PUT("/update/:isbn", func(c *gin.Context) {
			isbnRaw := c.Param("isbn")
			isbn := ISBN(isbnRaw)

			var book Book
			if err := c.BindJSON(&book); err != nil {
				fmt.Println(err)
				c.String(http.StatusInternalServerError, "Server Error")
				return
			}
			err := service.UpdateBook(isbn, book)
			if err != nil {
				c.String(http.StatusInternalServerError, "Server Error")
				return
			}
			c.JSON(http.StatusOK, gin.H{})
		})
		v1.DELETE("/delete/:isbn", func(c *gin.Context) {
			isbnRaw := c.Param("isbn")
			isbn := ISBN(isbnRaw)
			err := service.DeleteBook(isbn)
			if err != nil {
				c.String(http.StatusInternalServerError, "Server Error")
				return
			}
			c.JSON(http.StatusOK, gin.H{})
		})
		html := engine.Group("/html")
		{

			html.GET("/list", func(c *gin.Context) {
				list, err := service.FindAllBookList()
				if err != nil {
					c.String(http.StatusInternalServerError, "Server Error")
					return
				}
				c.HTML(http.StatusOK, "index.html",
					gin.H{"list": list})
			})
		}
	}
}
