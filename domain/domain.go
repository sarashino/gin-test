package domain

import "gorm.io/gorm"

type ISBN string

type Book struct {
	gorm.Model
	ISBN      ISBN   `gorm:"primaryKey" binding:"required"`
	Title     string `binding:"required"`
	BasePrice uint   `binding:"required"`
}

// if validation is implemented, it's favorable
// Entity{Validation()}
// (b *Book) Validate(){}
