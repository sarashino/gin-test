package main

import "gin_test/controller"

// import "./middleware"
import "github.com/gin-gonic/gin"

func main() {
	engine := gin.Default()
	engine.LoadHTMLGlob("templates/*")
	// engine.Use(middleware.Logger)
	controller.SetController(engine)
	engine.Run(":3000")
}
