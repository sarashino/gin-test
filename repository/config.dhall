let DBConfig
    : Type
    = { DBName : Text, Host : Text, Port : Natural }

let Stage
    : Type
    = < Local | Dev | Stg | Prd >

let stageToText =
      \(stage : Stage) ->
        merge
          { Local = "local"
          , Dev = "development"
          , Stg = "staging"
          , Prd = "production"
          }
          stage

let toURI =
      \(c : DBConfig) ->
        "host=${c.Host} dbname=${c.DBName} port=${Natural/show c.Port}"

in  { Stage, stageToText, DBConfig, toURI }
