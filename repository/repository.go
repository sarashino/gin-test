package repository

import . "gin_test/domain"
import "github.com/philandstuff/dhall-golang/v6"
import "gorm.io/driver/postgres"
import "gorm.io/gorm"

type repository struct {
	dbInterface
}

type dbInterface interface {
	FindAllBook()
	FindBookByISBN()
	CreateBook()
	UpdateBook()
	DeleteBookByISBN()
}

type BookRepository struct {
	db *gorm.DB
}

type Config struct {
    Stage string
    URI string
}

func NewBookRepository() BookRepository {
    var config Config
    err := dhall.UnmarshalFile("repository/config-local.dhall", &config)
	if err != nil {
		panic(err)
	}
	dsn := config.URI
	db, _ := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	return BookRepository{db}
}

func (r *BookRepository) FindAllBook() ([]Book, error) {
	var books []Book
	result := r.db.Find(&books)
	return books, result.Error
}
func (r *BookRepository) FindBookByISBN(isbn ISBN) (Book, error) {
	var book Book
	result := r.db.First(&book, "isbn = ?", isbn)
	// return specific error if non found
	return book, result.Error
}

func (r *BookRepository) CreateBook(book Book) error {
	result := r.db.Create(&book)
	return result.Error
}

func (r *BookRepository) UpdateBook(book Book) error {
	result := r.db.Save(&book)
	return result.Error
}
func (r *BookRepository) DeleteBookByISBN(isbn ISBN) error {
	result := r.db.Delete(&Book{}, "isbn = ?", isbn)
	// if result.RowsAffected is not 1
	return result.Error
}
