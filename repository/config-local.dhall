let Util = ./repository/config.dhall

let config
    : Util.DBConfig
    = { DBName = "gorm_test", Host = "localhost", Port = 5432 }

in  { Stage = Util.stageToText Util.Stage.Local, URI = Util.toURI config }
