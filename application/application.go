package application

import "fmt"
import "errors"
import . "gin_test/domain"
import . "gin_test/repository"

type Service struct {
	apiInterface
}

type apiInterface interface {
	FindBook()
	FindAllBookList()
	AddBook()
	UpdateBook()
	DeleteBook()
}

type BookService struct {
	bookRepository BookRepository
}

func NewBookService() BookService {
	repository := NewBookRepository()
	return BookService{repository}
}

func (s *BookService) FindBook(isbn ISBN) (Book, error) {
	book, err := s.bookRepository.FindBookByISBN(isbn)
	return book, err
}

func (s *BookService) FindAllBookList() ([]Book, error) {
	books, err := s.bookRepository.FindAllBook()
	return books, err
}

func (s *BookService) AddBook(book Book) error {
	err := s.bookRepository.CreateBook(book)
	return err
}

func (s *BookService) UpdateBook(isbn ISBN, book Book) error {
	if isbn != book.ISBN {
		// better return specific error
		return errors.New("invalid request")
	}
	err := s.bookRepository.UpdateBook(book)
	return err
}
func (s *BookService) DeleteBook(isbn ISBN) error {
	err := s.bookRepository.DeleteBookByISBN(isbn)
	return err
}

type BookServiceMock struct{}

func (s *BookServiceMock) FindBook(isbn ISBN) (Book, error) {
	book := Book{
		ISBN:      isbn,
		Title:     "title",
		BasePrice: 123,
	}
	return book, nil
}
func (s *BookServiceMock) FindAllBookList() ([]Book, error) {
	book := Book{
		ISBN:      ISBN("isbn"),
		Title:     "title",
		BasePrice: 123,
	}
	ls := []Book{book}
	return ls, nil
}

func (s *BookServiceMock) AddBook(book Book) error {
	fmt.Println("creating")
	fmt.Println(book)
	return nil
}

func (s *BookServiceMock) UpdateBook(isbn ISBN, book Book) error {
	fmt.Printf("updating %s to %s\n", isbn, book.ISBN)
	fmt.Println(book)
	return nil
}
func (s *BookServiceMock) DeleteBook(isbn ISBN) error {
	fmt.Printf("deleting %s\n", isbn)
	return nil
}
